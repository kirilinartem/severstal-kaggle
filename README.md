Train and some EDA.ipynb - Блокнот с тренировкой модели. Выполнялся в системе Kaggle так что, пути соответствуют файловой структуре Kaggle

Using model.ipynb - Блокнот с использованием модели на тестовых данных (data/sample_submission.csv)

data - папка с данными от Severstal

temp/UNET.h5 - Тренированая модель

temp/history_dict - История тренировки модели (то, что возвращает model.fit())

temp/processed_test.csv - CSV со структурой как у data/sample_submission.csv. Содержит информацию о дефектах тестовых изображений
