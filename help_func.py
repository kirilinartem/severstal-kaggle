def num2pix(number):
    y = number // 256
    x = (number - 1) % 256
    return (x,y)